﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Attributes
{
   
    [AttributeUsage(AttributeTargets.All, Inherited = false,
        AllowMultiple = true)]
    sealed class LastChangeAttribute : Attribute
    {
       public string _User { get; set; }
       public DateTime _CreationDate { get; set; }
       public int _Id { get; set; }


       // See the attribute guidelines at 
        //  http://go.microsoft.com/fwlink/?LinkId=85236
        public LastChangeAttribute(string User, string CreationDate, int Id)

        {
             _User = User;
            _CreationDate = DateTime.Parse(CreationDate);
            _Id = Id;

        }
    }
}
