﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attributes
{
    class Program
    {
        static void Main(string[] args)
        {
            Type t = typeof(SomeClass);
            System.Console.WriteLine("LastChangeAttribute information for {0}", t);

            System.Attribute[] attrs = System.Attribute.GetCustomAttributes(t);


            foreach (System.Attribute attr in attrs)
            {
                if (attr is LastChangeAttribute)
                {
                    LastChangeAttribute a = (LastChangeAttribute)attr;
                    Console.WriteLine($"Date edit: {a._CreationDate}, {a._User}, Number Task: {a._Id}");
                }
            }

            Console.ReadKey();

        }
    }
}
