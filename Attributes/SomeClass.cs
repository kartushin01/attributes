﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attributes
{
    [LastChange("Alex", "2019-12-02" , 11576)]
    class SomeClass
    {
        public SomeClass(int someNumber, string someText)
        {
            SomeNumber = someNumber;
            SomeText = someText;
        }

        public string SomeText { get; set; }
        public int SomeNumber { get; set; }


    }
}
